<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/hello', function() {
    return view('hello');
});*/
Route::get('/hello', 'Hello@index');
Route::get('/hello/{name}', 'Hello@show');
Route::get('/komentar', 'Komentar@index')->name('komentarIndex'); // show komentar list
Route::get('/komentar/new', 'Komentar@new_form')->name('komentarNewForm'); // show create new komentar form
Route::post('/komentar', 'Komentar@save')->name('komentarCreate'); // process create new komentar
// versi panjang
$a = Route::get('/komentar/{id}', 'Komentar@detail');
$a->name('komentarDetail'); // show detail komentar
Route::get('/komentar/delete/{id}', 'Komentar@delete')->name('komentarDelete'); // deletes komentar

Route::get('/komentar/edit/{id}', 'Komentar@edit')->name('komentarEditForm'); // show komentar edit form
Route::post('/komentar/edit/{id}', 'Komentar@update')->name('komentarUpdate'); // update komentar