<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// endpoints
Route::get('/hello', 'Api\Hello@index');
Route::get('/hello/{name}', 'Api\Hello@show');

Route::post('/todo', 'Api\Todo@create');
Route::get('/todo', 'Api\Todo@list');
Route::get('/todo/{id}', 'Api\Todo@get');
Route::patch('/todo/{id}', 'Api\Todo@update');
Route::delete('/todo/{id}', 'Api\Todo@delete');