'use strict';

const e = React.createElement;

class LikeButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { liked: false };
  }

  render() {
    if (this.state.liked) {
      return 'You liked this.';
    }

    return e(
      'button',
      { onClick: () => this.setState({ liked: true }) },
      'Like'
    );
  }
}

class HelloWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: 'World' };
    this.textbox = React.createRef();
  }

  render() {
    // text box
    const textbox = e('input',
      {type: 'text', ref: this.textbox, key: '1'});
    const button = e('button', 
      {onClick: () => {
        const value = this.textbox.current.value == '' ? 'World' : this.textbox.current.value;
        this.setState({name: value});
        this.textbox.current.value = '';
        this.textbox.current.focus();
       } , key: '2'},
      'Hello');
    const p = e('p', 
      {key: '3'}, 'Hello, ' + this.state.name)
    const div = e('div', null, [textbox, button, p]);
    return div;
  }
}

class BindWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: 'World' };
    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    const value = event.target.value == '' ? 'World' : event.target.value;
    this.setState({text: value })
  }

  render() {
    const textbox = e('input', {type: 'text', value: this.state.text, onChange: this.onChange, key: 1 })
    const p = e('p', {key: 2}, 'Hi ' + this.state.text)
    return e('div', null, [textbox, p])
  }
}

class HelloAjaxWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    const name = event.target.value == '' ? 'World' : event.target.value;
    this.setState({text: 'Waiting'});
    fetch('http://localhost:8000/api/hello/' + name)
      .then(res => res.json())
      .then(result => {
        this.setState({
          text: result.message
        })
      },
      error => {
        this.setState({
          text: 'Error'
        })
      });
  }

  render() {
    const textbox = e('input', {type: 'text', onChange: this.onChange, key: 1})
    const p = e('p', {key: 2}, this.state.text)
    return e('div', null, [textbox, p])
  }
}

const domContainer = document.querySelector('#like_button_container');
ReactDOM.render(e(LikeButton), domContainer);

const domContainer1 = document.querySelector('#hello_app');
ReactDOM.render(e(HelloWidget), domContainer1);

const domContainer2 = document.querySelector('#bind_app');
ReactDOM.render(e(BindWidget), domContainer2)

const domContainer3 = document.querySelector('#hello_ajax_app')
ReactDOM.render(e(HelloAjaxWidget), domContainer3)