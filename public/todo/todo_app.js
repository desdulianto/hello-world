'use strict';

const e = React.createElement;

class NewTodoWidget extends React.Component {
    constructor(props) {
        super(props);
        this.state = { description: '' };
        this.onChange = this.onChange.bind(this);
        this.onClick = this.onClick.bind(this);
    }

    onChange(event) {
        this.setState({description: event.target.value})
    }

    onClick(event) {
        this.props.onClick(event, this.state);
        this.setState({description: ''});
    }

    render() {
        return e('div', null, [
            e('label', {key: 1}, 'New Todo'),
            e('input', {key: 2, type: 'text', value: this.state.description,
                        onChange: this.onChange, placeholder: 'New Todo'}),
            e('button', {key: 3, onClick: this.onClick}, 'Add')
        ]);
    }
}

class TodoItem extends React.Component {
    constructor(props) {
        super(props);
        this.onDelete = this.onDelete.bind(this);
        this.onStarted = this.onStarted.bind(this);
        this.onFinish = this.onFinish.bind(this);
        this.onUndo = this.onUndo.bind(this);
        this.buildButtons = this.buildButtons.bind(this);

        this.deleteButton = e('button', {key: 1, onClick: this.onDelete}, 'Delete');
        this.startButton = e('button', {key: 2, onClick: this.onStarted}, 'Start');
        this.finishButton = e('button', {key: 2, onClick: this.onFinish}, 'Finish');
        this.undoButton =  e('button', {key: 3, onClick: this.onUndo}, 'Undo');
    }

    onDelete(event) {
        if (confirm('Are you sure?')) {
            this.props.onDelete(event, this.props.value.id);
        }
    }

    onStarted(event) {
        this.props.onStarted(event, this.props.value.id);
    }

    onFinish(event) {
        this.props.onFinish(event, this.props.value.id);
    }

    onUndo(event) {
        this.props.onUndo(event, this.props.value.id);
    }

    getState(todo) {
        if (todo.finished !== null) {
            return 'finished';
        } else if (todo.started !== null) {
            return 'started';
        } else {
            return 'created';
        }
    }

    buildButtons(todo) {
        return [
            this.deleteButton
        ].concat([
            this.getState(todo) === 'created' ? this.startButton :
            this.getState(todo) === 'started' ? this.finishButton :
            ''
        ]).concat([
            this.getState(todo) !== 'created' ? this.undoButton : ''
        ]);
    }

    render() {
        var todo = this.props.value;
        var state = this.getState(todo);
        var bgColor = {finished: 'green', started: 'cyan', created: 'yellow'}[state];
        var text = '(' + this.props.value.id + ') ' + this.props.value.description;
        return e('li', {style: {backgroundColor: bgColor}}, [
            text, ' ',
            this.buildButtons(todo),
        ]);
    }
}

class TodoList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var items = this.props.value.map(
            (i, j) => e(TodoItem, {
                value: i,
                key: j,
                onDelete: this.props.onDelete,
                onStarted: this.props.onStarted,
                onFinish: this.props.onFinish,
                onUndo: this.props.onUndo
            }));
        return e('ul', null, items);
    }
}

class TodoApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {todos: []};
        this.newTodo = this.newTodo.bind(this);
        this.loadTodos = this.loadTodos.bind(this);
        this.deleteTodo = this.deleteTodo.bind(this);
        this.buildTodoStatusHandler = this.buildTodoStatusHandler.bind(this);
        this.startTodo = this.buildTodoStatusHandler('started');
        this.startTodo = this.startTodo.bind(this);
        this.finishTodo = this.buildTodoStatusHandler('finished');
        this.finishTodo = this.finishTodo.bind(this);
        this.undoTodo = this.buildTodoStatusHandler('undo');
        this.undoTodo = this.undoTodo.bind(this);
        this.baseURL = "http://localhost:8000/api/todo";
    }

    loadTodos() {
        fetch(this.baseURL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).
        then(res => res.json()).
        then(json => this.setState({todos: json.data})).
        catch(e => alert(e));
    }

    componentDidMount() {
        this.loadTodos();
    }

    newTodo(event, data) {
        fetch(this.baseURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).
        then(res => res.json()).
        then(json => {
            var todos = this.state.todos.concat([json.data]);
            this.setState({todos: todos});
        }).
        catch(e => alert(e));
    }

    deleteTodo(event, id) {
        fetch(this.baseURL + '/' + id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }).
        then(res => {
            var todos = this.state.todos.filter(i => i.id !== id)
            this.setState({todos: todos});
        }).
        catch(e => alert(e));
    }

    /*startTodo(event, id) {
        fetch(this.baseURL + '/' + id, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({status: 'started'})
        }).
        then(res => res.json()).
        then(json => {
            var todos = this.state.todos.map(i => i.id === id ? json.data : i)
            this.setState({todos: todos});
        }).
        catch(e => alert(e));
    }

    finishTodo(event, id) {
        fetch(this.baseURL + '/' + id, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({status: 'finished'})
        }).
        then(res => res.json()).
        then(json => {
            var todos = this.state.todos.map(i => i.id === id ? json.data : i)
            this.setState({todos: todos});
        }).
        catch(e => alert(e));
    }*/

    buildTodoStatusHandler(status) {
        return function(event, id) {
            fetch(this.baseURL + '/' + id, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({status: status})
            }).
            then(res => res.json()).
            then(json => {
                var todos = this.state.todos.map(i => i.id === id ? json.data : i)
                this.setState({todos: todos});
            }).
            catch(e => alert(e));
        }
    }

    render() {
        return e('div', null, [
            e(NewTodoWidget, {key: 1, onClick: this.newTodo}),
            e(TodoList, {
                key: 2,
                value: this.state.todos,
                onDelete: this.deleteTodo,
                onStarted: this.startTodo,
                onFinish: this.finishTodo,
                onUndo: this.undoTodo
            })
        ]);
    }
}

const app = document.querySelector('#todo_app');
ReactDOM.render(e(TodoApp), app);