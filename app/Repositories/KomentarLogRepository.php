<?php

namespace App\Repositories;

use App\Repositories\KomentarInterface;
use App\Komentar;
use Illuminate\Support\Facades\Log;

class KomentarLogRepository implements KomentarInterface {
    private $repo;

	public function __construct($repo) {
		$this->repo = $repo;
	}

    public function all() {
        Log::info('retrieve all komentar');
        return $this->repo->all();
    }

    public function create(string $nama, string $komentar) {
        Log::info('create new komentar');
        return $this->repo->create($nama, $komentar);
    }

    public function get(int $id) {
        Log::info("retrieve komentar with id $id");
        return $this->repo->get($id);
    }

    public function update(int $id, Komentar $komentar) {
        Log::info("updating komentar with id $id");
        return $this->repo->update($id, $komentar);
    }
    
    public function delete(int $id) {
        Log::info("delete komentar with id $id");
        return $this->repo->delete($id);
    }
}
