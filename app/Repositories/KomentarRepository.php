<?php

namespace App\Repositories;

use App\Komentar;

class KomentarRepository implements KomentarInterface {
    public function create(string $nama, string $komentar) {
        $newKomentar = new Komentar;
        $newKomentar->nama = $nama;
        $newKomentar->komentar = $komentar;
        $newKomentar->save();
    }

    public function all() {
        return Komentar::all();
    }

    public function get(int $id) {
        $komentar = Komentar::find($id);
		return $komentar;
    }

    public function update(int $id, Komentar $data) {
    }

    public function delete(int $id) {
    }
}
