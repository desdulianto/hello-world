<?php

namespace App\Repositories;

use App\Komentar;

interface KomentarInterface {
	// create new komentar data
    public function create(string $nama, string $komentar);
	// retrieve all komentar
    public function all();
	// get komentar by id
    public function get(int $id);
	// update komentar by id
    public function update(int $id, Komentar $data);
	// delete komentar by id
    public function delete(int $id);
}
