<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Hello extends Controller
{
    public function index(Request $request) {
        sleep(1);
        return ["message"=>"Hello world"];
    }

    public function show($name) {
        return ["message"=>"Hello $name from server"];
    }
}
