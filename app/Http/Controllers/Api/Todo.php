<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\todo as TodoModel;

class Todo extends Controller
{
    public function create(Request $request)
    {
        if (!$request->has('description')) {
            return response()->json(
                ['message'=>'missing description data'],
                400);
        }
        $description = $request->input('description');
        if (empty($description)) {
            return response()->json(
                ['message'=>'empty description data'],
                400);
        }

        $todo = new TodoModel;
        $todo->description = $description;
        $todo->created = Carbon::now();
        $todo->started = null;
        $todo->finished = null;
        try {
            $todo->save();
        } catch(Exception $e) {
            return response()->json(
                ['error'=>$e->getMessage()],
                500);
        }

        return response()->json(["data"=>$todo], 201);
    }

    public function get($id)
    {
        $todo = TodoModel::find($id);
        if ($todo == null) {
            return response()->json(['message'=>'todo ' . $id . ' not found'], 404);
        }
        return response()->json(["data"=>$todo], 200);
    }

    public function list(Request $request)
    {
        $offset = 0;
        $limit = 100;
        $query = TodoModel::offset($offset)->limit($limit);

        $keyword = $request->query('keyword');
        if ($keyword != '') {
            $query = $query->where('description', 'like', '%'.$keyword.'%');
        }
        
        $status = $request->query('status');
        switch ($status) {
            case 'created':
                $query = $query->whereNotNull('created');
                break;
            case 'started':
                $query = $query->whereNotNull('started');
                break;
            case 'finished':
                $query = $query->whereNotNull('finished');
                break;
        }

        $todos = $query->get();
        return response()->json(["data"=>$todos], 200);
    }

    public function update(Request $request, $id)
    {
        $todo = TodoModel::find($id);
        if ($todo == null) {
            return response()->json(['message'=>'Todo ' .$id. ' not found'], 404);
        }

        $description = $request->input('description');
        if ($description == '') {
            $description = $todo->description;
        }
        $status = $request->input('status');
        $started = $todo->started;
        $finished = $todo->finished;
        switch ($status) {
            case 'started':
                $started = Carbon::now();
                break;
            case 'finished':
                $finished = Carbon::now();
                break;
            case 'undo':
                if ($finished != null) {
                    $finished = null;
                } else if ($started != null) {
                    $started = null;
                }
        }

        $todo->description = $description;
        $todo->started = $started;
        $todo->finished = $finished;
        try {
            $todo->save();
        } catch (Exception $e) {
            return response()->json(['error'=>$e->getMessage()], 500);
        }

        return response()->json(['data'=>$todo], 200);

    }

    public function delete($id) {
        $todo = TodoModel::find($id);
        if ($todo == null) {
            return response()->json(['message'=>'todo ' .$id. ' not found'], 404);
        }
        try {
            $todo->delete();
        } catch (Exception $e) {
            return response()->json(['error'=>$e->getMessage()], 500);
        }

        return response()->json(null, 204);
    }
}
