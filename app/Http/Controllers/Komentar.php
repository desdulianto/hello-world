<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Komentar as KomentarModel;
use App\Repositories\KomentarInterface;

class Komentar extends Controller
{
    protected $komentarRepo;

    public function __construct(KomentarInterface $repo) {
        $this->komentarRepo = $repo;
    }

    // list all komentars
    public function index() {
        $komentars = $this->komentarRepo->all();
        return view('komentar.index',
            ['komentars'=>$komentars]);
    }

    // return create new komentar form
    public function new_form() {
        return view('komentar.new');
    }

    // create/save komentar
    public function save(Request $request) {
        $nama = $request->input('nama');
        $komentar = $request->input('komentar');
        $this->komentarRepo->create($nama, $komentar);
        return redirect(route('komentarIndex'));
    }

    // view komentar detail
    public function detail(int $id) {
        /*$komentar = KomentarModel::find($id);
        if ($komentar == null) { 
            abort(404);
        }*/
        $komentar = $this->komentarRepo->get($id);
		if ($komentar == null) {
			abort(404);
		}
        return view('komentar.detail',
            ['komentar'=>$komentar]);
    }

    // return edit komentar form
    public function edit(int $id) {
        $komentar = KomentarModel::findOrFail($id);
        return view('komentar.edit',
            ['komentar'=>$komentar]);
    }

    // update komentar
    public function update(Request $request, int $id) {
        $komentar = KomentarModel::findOrFail($id);
        $komentar->nama = $request->input('nama');
        $komentar->komentar = $request->input('komentar');
        $komentar->save();
        return redirect(route('komentarIndex'));
    }

    // delete komentar
    public function delete(int $id) {
        $komentar = KomentarModel::findOrFail($id);
        $komentar->delete();
        return redirect(route('komentarIndex'));
    }
}
