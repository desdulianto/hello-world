@extends('base')

@section('body')
<button ><a href="{{ route('komentarNewForm') }}">Add New</a></button>
<table>
    <tr>
        <th>ID</th>
        <th>Nama</th>
        <th>Komentar</th>
        <th>Action</th>
    </tr>
    @foreach ($komentars as $komentar)
    <tr>
        <td><a href="{{ route('komentarDetail', ['id' => $komentar->id]) }}">{{ $komentar->id }}</a></td>
        <td><a href="">{{ $komentar->nama }}</a></td>
        <td><a href="">{{ $komentar->komentar }}</a></td>
        <td><a href="{{ route('komentarDelete', ['id' => $komentar->id]) }}"
            onclick="return confirm('Are you sure?')">Delete</a>
            <a href="{{ route('komentarEditForm', ['id'=>$komentar->id]) }}">Edit</a>
        </td>
    </tr>
    @endforeach
</table>
@endsection