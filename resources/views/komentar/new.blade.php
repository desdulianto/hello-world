@extends('base')

@section('body')
<form method="post" action="{{ route('komentarCreate') }}">
    @csrf
    <div>
        <label>Nama:</label>
        <input type="text" name="nama">
    </div>
    <div>
        <label>Komentar:</label>
        <input type="text" name="komentar">
    </div>
    <div>
        <input type="submit">
    </div>
</form>
@endsection