@extends('base')

@section('body')
<form method="post" action="{{ route('komentarUpdate', ['id'=>$komentar->id]) }}">
    @csrf
    <div>
        <label>Nama:</label>
        <input type="text" name="nama" value="{{ $komentar->nama }}">
    </div>
    <div>
        <label>Komentar:</label>
        <input type="text" name="komentar" value="{{ $komentar->komentar }}">
    </div>
    <div>
        <input type="submit">
    </div>
</form>
@endsection