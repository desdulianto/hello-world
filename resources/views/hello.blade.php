<!-- resources/views/hello.blade.php -->
@extends('base')

@section('body')
    <h1>Hello {{ $name }}</h1>
    <h2>Bye {{ $name }}</h2>
@endsection